<?php
/*
 * Template Name: Services
*/
get_header(); 


@include 'sections/services/services-intro.php';
@require 'sections/services/services-content.php';
@include 'sections/layout/call-to-action.php';


get_footer(); ?>