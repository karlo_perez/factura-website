<?php
/*
 * Template Name: Tags Page
 * 
*/

get_header();


@require 'sections/layout/tag-intro.php';
@require 'sections/layout/tag-content.php';
@include 'sections/layout/call-to-action.php';


get_footer(); ?>