<?php
/*
 * Template Name: Home
*/
get_header(); 

@include 'sections/home/intro.php';
@include 'sections/home/summary.php';
// @include 'sections/home/preview-projects.php'; hidden for 1st pass
@include 'sections/home/preview-services.php';
@include 'sections/layout/call-to-action.php';

get_footer(); ?>