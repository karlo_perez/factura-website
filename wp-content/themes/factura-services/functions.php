<?php
// CUSTOM STYLING
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}



// CUSTOM MENU for MAIN NAVIGATION
function wpb_custom_new_menu() {
    register_nav_menu('custom-menu',__( 'Custom Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );



// WEBSITE LOGO via APPEARANCE MENU
add_theme_support( 'custom-logo', array(
	'height'      => 100,
	'width'       => 400,
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
) );



// LIMIT EXCERPTS CHARACTERS before READ MORE is DISPLAYED
function wpdocs_custom_excerpt_length( $length ) {
    return 17;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


// CHANGE EXCERPTS to MORE
function new_excerpt_more($more) {
	global $post;
	return '… <a href="'. get_permalink($post->ID) . '">' . 'Read more <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.26 26.58">
	<path class="a" d="M11.88,17.49a2,2,0,0,0,0-2.8l-2.81-2.8L.58,3.37A2,2,0,0,1,3.37.58L14.69,11.89a2,2,0,0,1,0,2.8l-2.81,2.8"/>
	<path class="a" d="M.58,23.2l8.51-8.51a2,2,0,0,0,.58-1.42L11.37,15c.23.22,1.63,1.39-1,4.07L3.37,26A2,2,0,1,1,.58,23.2Z"/>
	</svg>' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


// ADD EXCERPTS for PAGES
add_post_type_support( 'page', 'excerpt' );


// ADD "EDIT IN NEW TAB" on POSTS ADMIN PANEL
add_filter( 'post_row_actions', 'tab_row_actions', 10, 2 );
function tab_row_actions( $actions, $post )
{
    if( get_post_type() === 'post' )
	{
		$edit_tag = "<a target='_blank' href='";
		$link = admin_url( 'post.php' );
		$edit_tag .= add_query_arg(array('post' => $post->ID, 'action' => 'edit'), $link);
		$edit_tag .= "'>Edit in New Tab</a>";
		$actions['edit_new_tab'] = $edit_tag;
	}
    return $actions;
}



// FEATURED IMAGE of POSTS
add_theme_support( 'post-thumbnails' );
// remove width & height attributes from images
//
function remove_img_attr ($html)
{
    return preg_replace('/(width|height)="\d+"\s/', "", $html);
}
 
add_filter( 'post_thumbnail_html', 'remove_img_attr' );



// FILTER POSTS without TAGS
function wpse147471_add_no_tags_filter() {
    if ( 'post' !== get_current_screen()->post_type ) {
        return;
    }

    $selected = ( isset( $_GET['tag-filter'] ) && 'no-tags' === $_GET['tag-filter'] );
    ?>
    <select name="tag-filter">
        <option value="">&mdash; Select &mdash;</option>
        <option value="no-tags" <?php echo selected( $selected ); ?>>No Tags</option>
    </select>
    <?php
}
add_action( 'restrict_manage_posts', 'wpse147471_add_no_tags_filter' );

function wpse147471_get_posts_with_no_tags( $query ) {
    if ( ! is_admin() || ! $query->is_main_query() ) {
        return;
    }

    if ( ! isset( $_GET['tag-filter'] ) || 'no-tags' !== $_GET['tag-filter'] ) {
        return;
    }

    $tag_ids = get_terms( 'post_tag', array( 'fields' => 'ids' ) );

    $query->set( 'tax_query', array(
        array(
            'taxonomy' => 'post_tag',
            'field'    => 'id',
            'terms'    => $tag_ids,
            'operator' => 'NOT IN'
        )
    ) );
}
add_action( 'pre_get_posts', 'wpse147471_get_posts_with_no_tags' );



// PAGINATION for POSTS
function pagination_bar( $custom_query ) {

    $total_pages = $custom_query->max_num_pages;
    $big = 999999999; // need an unlikely integer

    if ( $total_pages > 1 ) {
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}



// POSTS DUPLICATOR
/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/* Nonce verification */
	if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
		return;
 
	/* get the original post id */
	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
	/* and all the original post data then */
	$post = get_post( $post_id );
 
	/* if you don't want current user to be the new post author, then change next couple of lines to this: $new_post_author = $post->post_author; */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/* if post data exists, create the post duplicate */
	if (isset( $post ) && $post != null) {
 
		/* new post data array */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/* insert the post by wp_insert_post() function */
		$new_post_id = wp_insert_post( $args );
 
		/* get all current post terms ad set them to the new post draft */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/* duplicate all post meta just in two SQL queries */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/* finally, redirect to the edit post screen for the new draft */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
 
/* Add the duplicate link to action list for post_row_actions */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
// add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );
// // // USE THIS TO DUPLICATE PAGES INSTEAD OF POSTS // // //
add_filter('page_row_actions', 'rd_duplicate_post_link', 10, 2);



// WIDGETS via APPEARANCE
function wpb_custom_sidebar() {
	register_sidebar( array(
        'name' => 'Logo',
        'id' => 'logo-container',
        'description' => 'Appears leftmost in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) 
	);
    register_sidebar( array(
        'name' => 'Site Map',
        'id' => 'site-map',
        'description' => 'Appears next to logo in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) 
	);
	register_sidebar( array(
        'name' => 'Social Media',
        'id' => 'social-media',
        'description' => 'Appears next to map in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) 
	);
	register_sidebar( array(
        'name' => 'Address',
        'id' => 'address',
        'description' => 'Appears next to map in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) 
	);
	register_sidebar( array(
        'name' => 'Phone',
        'id' => 'phone',
        'description' => 'Appears next to map in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) 
	);
	register_sidebar( array(
        'name' => 'Google Map',
        'id' => 'google-map',
        'description' => 'Appears next to map in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) 
	);
	register_sidebar( array(
        'name' => 'Copyright',
        'id' => 'copyright',
        'description' => 'Appears next to map in the footer area',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        ) 
	);
}
add_action( 'widgets_init', 'wpb_custom_sidebar' );


// ALLOWING of [.webp] image files
/**
 * Sets the extension and mime type for .webp files.
 *
 * @param array  $wp_check_filetype_and_ext File data array containing 'ext', 'type', and
 *                                          'proper_filename' keys.
 * @param string $file                      Full path to the file.
 * @param string $filename                  The name of the file (may differ from $file due to
 *                                          $file being in a tmp directory).
 * @param array  $mimes                     Key is the file extension with value as the mime type.
 */
add_filter( 'wp_check_filetype_and_ext', 'wpse_file_and_ext_webp', 10, 4 );
function wpse_file_and_ext_webp( $types, $file, $filename, $mimes ) {
    if ( false !== strpos( $filename, '.webp' ) ) {
        $types['ext'] = 'webp';
        $types['type'] = 'image/webp';
    }

    return $types;
}

/**
 * Adds webp filetype to allowed mimes
 * 
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 * 
 * @param array $mimes Mime types keyed by the file extension regex corresponding to
 *                     those types. 'swf' and 'exe' removed from full list. 'htm|html' also
 *                     removed depending on '$user' capabilities.
 *
 * @return array
 */
add_filter( 'upload_mimes', 'wpse_mime_types_webp' );
function wpse_mime_types_webp( $mimes ) {
    $mimes['webp'] = 'image/webp';

  return $mimes;
}



// PREVENT MULTI/SPAM SUBMISSION ON ALL WPCF7 FORMS
add_action( 'wp_footer', 'prevent_cf7_multiple_emails' );

function prevent_cf7_multiple_emails() {
?>

	<script type="text/javascript">
	var disableSubmit = false;
	jQuery('input.wpcf7-submit[type="submit"]').click(function() {
		jQuery(':input[type="submit"]').attr('value',"Sending...");
		if (disableSubmit == true) {
			return false;
		}
		disableSubmit = true;
		return true;
	})
	
	var wpcf7Elm = document.querySelector( '.wpcf7' );
	wpcf7Elm.addEventListener( 'wpcf7_before_send_mail', function( event ) {
		jQuery(':input[type="submit"]').attr('value',"Sent");
		disableSubmit = false;
	}, false );

	wpcf7Elm.addEventListener( 'wpcf7invalid', function( event ) {
		jQuery(':input[type="submit"]').attr('value',"Submit")
		disableSubmit = false;
	}, false );
	</script>
<?php
}
?>