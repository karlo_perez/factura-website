<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>

    <title><?php echo bloginfo('name'); ?></title>

    

    <!-- // // // EXTERNAL STYLESHEET LINKS [START] // // // -->    
    <!-- 1) Cherry-pick bootstrap functionalities -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->
    <!-- 2) replacing FA icons with local ones to avoid adding external requests -->
    <!-- <script src="https://kit.fontawesome.com/872cdcfc09.js" crossorigin="anonymous"></script> -->
    <!-- 3) Flickity stylesheet -->
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/vendors/flickity.css">
    <!-- 4) Flickity-fade stylesheet -->
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/vendors/flickity-fade.css">
    <!-- // // // EXTERNAL STYLESHEET LINKS [END] // // // -->

    <!-- // // // INTERNAL STYLESHEET LINKS [START] // // // -->
    <!-- 1) Concat "~.min.css" at the eng of stylesheet URL upon minifying -->
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/main.css">
    <!-- // // // INTERNAL STYLESHEET LINKS [END] // // // -->

	<?php wp_head(); ?>
</head>

<header>
    <div class="header-container">

        <div class="logo-container">

            <a class="logo-container__link" href="<?= get_home_url(); ?>" rel="noreferrer noopener">

                <img class="logo-container__image--full" src="<?= get_template_directory_uri(); ?>/media/logos/logo_Factura (horizontal).png" alt="">

                <?php // if ( is_front_page() || is_single() || is_page("company") ) : ?>

                    <!-- <img class="logo-container__image--white" src="<?php // echo get_template_directory_uri(); ?>/media/logos/logo_Factura (white horizontal).png" alt=""> -->
                    
                    <!-- <img class="logo-container__image--full hidden" src="<?php // echo get_template_directory_uri(); ?>/media/logos/logo_Factura (horizontal).png" alt=""> -->

                <?php // else: ?>

                    <!-- <img class="logo-container__image--white hidden" src="<?php // echo get_template_directory_uri(); ?>/media/logos/logo_Factura (white horizontal).png" alt=""> -->

                    <!-- <img class="logo-container__image--full" src="<?php // echo get_template_directory_uri(); ?>/media/logos/logo_Factura (horizontal).png" alt=""> -->

                <?php // endif; ?>
                
            </a>

        </div>

        <nav id="main-navigation" class="main-nav">
            <?php
            wp_nav_menu( array( 
                'theme_location' => 'custom-menu', 
                'container_class' => 'custom-menu-class'
            ) ); 
            ?>
        </nav>

        <a href="javascript:void(0);" class="mobile-icon" id="mobile-view-button" rel="noreferrer noopener" role="navigation">
            <img class="icon-open-mobile-nav" src="<?= get_stylesheet_directory_uri(); ?>/media/icons/noun_menu collapsed.svg" width="16" height="14" alt="">
            <img class="icon-close-mobile-nav" src="<?= get_stylesheet_directory_uri(); ?>/media/icons/noun_menu expanded.svg" width="16" height="14" alt="">
        </a>
    </div>
</header>

<?php if ( !is_front_page() && !is_single() && !is_page("company") ) : ?>
<div class="header-spacer"></div>
<?php endif; ?>

