<footer>
  <div class="footer-container">

    <div id="logo-container" class="footer-widget">
      <?php
      if(is_active_sidebar('logo-container')){
      dynamic_sidebar('logo-container');
      }
      ?>
    </div>

    <div id="site-map" class="footer-widget">
      <?php
      if(is_active_sidebar('site-map')){
      dynamic_sidebar('site-map');
      }
      ?>
    </div>
    
    <div id="links-and-contacts">

      <div id="social-media" class="footer-widget">
        <?php
        if(is_active_sidebar('social-media')){
        dynamic_sidebar('social-media');
        }
        ?>
      </div>

      <div id="address" class="footer-widget">
        <img src="<?= get_template_directory_uri(); ?>/media/icons/fa_icon_map marker.svg" alt="" class="icon-address">
        <?php
        if(is_active_sidebar('address')){
        dynamic_sidebar('address');
        }
        ?>
      </div>

      <div id="phone" class="footer-widget">
        <img src="<?= get_template_directory_uri(); ?>/media/icons/fa_icon_phone.svg" alt="" class="icon-phone">
        <?php
        if(is_active_sidebar('phone')){
        dynamic_sidebar('phone');
        }
        ?>
      </div>

    </div>

    <div id="google-map" class="footer-widget">
      <?php
      if(is_active_sidebar('google-map')){
      dynamic_sidebar('google-map');
      }
      ?>
    </div> 

  </div>

  <div id="copyright" class="footer-widget">
    <?php
    if(is_active_sidebar('copyright')){
    dynamic_sidebar('copyright');
    }
    ?>
  </div>
  
</footer>



<!-- // // // EXTERNAL SCRIPT LINKS [START] // // // -->
<!-- 1)  -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<!-- 2) JavaScript Bundle with Popper -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> -->
<!-- 3) <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->

<!-- <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script> -->
<!-- // // // EXTERNAL SCRIPT LINKS [END] // // // -->


<!-- // // // INTERNAL SCRIPT LINKS [START] // // // -->
<!-- jQuery compressed -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/vendors/jquery-compressed.js"></script>
<!-- Flickity script -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/vendors/flickity.pkgd.min.js"></script>
<!-- Flickity-fade script -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/vendors/flickity-fade.js"></script>
<!-- Rellax script -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/vendors/rellax.min.js"></script>
<!-- // // // INTERNAL SCRIPT LINKS [END] // // // -->




<!-- // // // // // // // // // // // // // // -->
<!-- // // // FOOTER SCRIPTS [START] // // // -->
<!-- // // // // // // // // // // // // // // -->

<!-- CHECK IF PAGE IS HOME PAGE, SINGLE POSTS: SERVICES, SINGLE POSTS: TAGS, and COMPANY -->
<!-- if page isn't the aformentioned, do not run the script in changing the header style upon scrolling -->
<!-- if page isn't the aformentioned, change the initial styling of the header nevertheless -->


<!-- SWITCH STATEMENT -->
<script>
// // CHANGE HEADER STYLE ON SCROLL TO A CERTAIN POINT // //
// window.addEventListener('scroll', changeHeaderStyleOnScroll);
// should work only on media query larger than tablet-portrait;
// function changeHeaderStyleOnScroll() {
//   var header = document.querySelector('.header-container');
//   var intro = document.querySelector('.offset-intro');
//   var ww = document.body.clientWidth;

//   // if(!header)return;
//   var introHeight = intro.offsetHeight - header.offsetHeight;
//   var scrollPos = window.scrollY;
//   var headerLogoWhite = document.querySelector('.logo-container__image--white');
//   var headerLogoFull = document.querySelector('.logo-container__image--full');

//   if (ww > 784) {
//     if (scrollPos >= introHeight) {
//       header.classList.add('header--alter');
//       headerLogoWhite.style.display = 'none';
//       headerLogoFull.style.display = 'flex';
//     } else {
//       header.classList.remove('header--alter');
//       headerLogoWhite.style.display = 'flex';
//       headerLogoFull.style.display = 'none';
//     }
//   } else {
//     header.classList.add('header--alter');
//     headerLogoWhite.style.display = 'none';
//     headerLogoFull.style.display = 'flex';
//   }
  
// };
// // SET HEADER STYLE depending on usage // //
// prevent this function from firing under pages it must not exist//
// function setHeaderStyle() {
//   var header = document.querySelector('.header-container');
//   var intro = document.querySelector('.home-intro');
//   var ww = document.body.clientWidth;
//   var headerLogoWhite = document.querySelector('.logo-container__image--white');
//   var headerLogoFull = document.querySelector('.logo-container__image--full');

//   if (ww > 784) {
//     header.classList.add('header--alter');
//     headerLogoWhite.style.display = 'none';
//     headerLogoFull.style.display = 'flex';
//   }
// }
</script>
<!-- CHANGE and SET HEADER STYLE depending on current page -->
<!-- needs to b refactored that the pages should not be explicit and instead, dependent on the page/post type -->
<?php
// $page = get_the_ID();

// switch ( $page ) {
//   case ( is_tag() ):
//     echo '<script>console.log("This page is the tag page");</script>';
//     echo '<script>setHeaderStyle();</script>';
//     break;
//   case ( is_single() ) :
//     echo '<script>console.log("This page is a category page");</script>';
//     echo '<script>window.addEventListener("scroll", changeHeaderStyleOnScroll);</script>';
//     break;

//   case ( is_front_page() ) :
//     echo '<script>console.log("This page is the front page");</script>';
//     echo '<script>window.addEventListener("scroll", changeHeaderStyleOnScroll);</script>';
//     break;
//   case ( is_page("projects") ) :
//     echo '<script>console.log("This page is the Projects page");</script>';
//     echo '<script>setHeaderStyle();</script>';
//     break;
//   case ( is_page("services") ) :
//     echo '<script>console.log("This page is the Services page");</script>';
//     echo '<script>setHeaderStyle();</script>';
//     break;
//   case ( is_page("company") ) :
//     echo '<script>console.log("This page is the Company page");</script>';
//     echo '<script>window.addEventListener("scroll", changeHeaderStyleOnScroll);</script>';
//     break;
//   case ( is_page("careers") ) :
//     echo '<script>console.log("This page is the Careers page");</script>';
//     echo '<script>setHeaderStyle();</script>';
//     break;
//   case ( is_page("contact") ) :
//     echo '<script>console.log("This page is the Contact page");</script>';
//     echo '<script>setHeaderStyle();</script>';
//     break;
//   default :
//   echo '<script>console.log("This page is unknown");</script>';
//     echo '<script>window.addEventListener("scroll", changeHeaderStyleOnScroll);</script>';
// } 
// ?>

<script>

// // SHOW ON SCROLL [START] // //
/* used mainly for animation */
var scroll = window.requestAnimationFrame ||
  // IE Fallback
  function(callback){ window.setTimeout(callback, 1000/60)};
var elementsToShow = document.querySelectorAll('.show-on-scroll'); 

function loop() {

  elementsToShow.forEach(function (element) {
    if (isElementInViewport(element)) {
      element.classList.add('is-visible');
    } else {
      // prevent animation from looping once the page loads once
      // element.classList.remove('is-visible');
    }
  });

  scroll(loop);
}
loop();

function isElementInViewport(el) {
  if (typeof jQuery === "function" && el instanceof jQuery) {
    el = el[0];
  }
  var rect = el.getBoundingClientRect();
  return (
    (rect.top <= 0
      && rect.bottom >= 0)
    ||
    (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.top <= (window.innerHeight || document.documentElement.clientHeight))
    ||
    (rect.top >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
  );
}
// // SHOW ON SCROLL [END] // //

// TEST //
// alert("javascript works!");

// // MOBILE RESPONSE [START] // //
// ALTER MAIN NAVIGATION DEPENDING ON SCREEN-SIZE
jQuery(document).ready(function($) {

var alterClass = function() {
  var ww = document.body.clientWidth;
  /* bug in which the resize is affected on 784px (tablet-portait) */
  if (ww > 784) {
    $('#main-navigation').removeClass('mobile-nav');
    $('#main-navigation').removeClass('mobile-nav-open');
    // $('body').removeClass('lock-scroll');
    // ! adding the class="lock-scroll" unto the body bugs the removal of class="mobile-nav-open" when resizing the screen; therefore must be removed from the body when resizing !

  } else {
    $('#main-navigation').addClass('mobile-nav');
    $('#main-navigation').removeClass('mobile-nav-open');
    // $('body').removeClass('lock-scroll');
    // ! adding the class="lock-scroll" unto the body bugs the removal of class="mobile-nav-open" when resizing the screen; therefore must be removed from the body when resizing !
  };
};

$(window).resize(function(){
  alterClass();
});

// TOGGLES
/* Show and hide elements via a toggle element */
$('#mobile-view-button').click(function() { 
  if ($('#main-navigation').hasClass('mobile-nav-open')){
    $('#main-navigation').removeClass('mobile-nav-open');
    $('body').removeClass('lock-scroll');
  } else {
    $('#main-navigation').addClass('mobile-nav-open');
    /* prevents the page from scrolling whenever mobile navigation menu is open */
    $('body').addClass('lock-scroll');
  };
});

alterClass();
});

// // MOBILE RESPONSE [END] // //


// // // FLICKITY // // //
$('#flickity-summary').flickity({
  wrapAround: true,
  prevNextButtons: false
});
</script>
<!-- // // // // // // // // // // // // // // -->
<!-- // // // // FOOTER SCRIPTS [END] // // // -->
<!-- // // // // // // // // // // // // // // -->


<?php wp_footer(); ?>

</body>
</html>