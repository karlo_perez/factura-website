<?php
/*
 * Template Name: Single Post Team
 * Template Post Type: Post
*/
get_header();


@include 'sections/company/single-post-team-intro.php';
@require 'sections/company/single-post-team-content.php';
@include 'sections/layout/call-to-action.php';


get_footer(); ?>