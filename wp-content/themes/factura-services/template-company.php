<?php
/*
 * Template Name: Company
*/
get_header(); 


@include 'sections/company/company-intro.php';
@require 'sections/company/company-content.php';
@include 'sections/company/company-team.php';
@include 'sections/layout/call-to-action.php';


get_footer(); ?>