<?php
/*
 * Template Name: Single Post Service
 * Template Post Type: Post
*/
get_header();


@include 'sections/services/single-post-service-intro.php';
@require 'sections/services/single-post-service-content.php';
@include 'sections/layout/call-to-action.php';


get_footer(); ?>