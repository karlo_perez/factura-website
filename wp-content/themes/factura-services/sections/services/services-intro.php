<div class="block--full services-intro">

    <div class="block-container">
    
    
        <div class="block-title block-title--left">

            <?php
                $servicesTitleGroup = get_field('services_title_group');
                $servicesTitleMain = $servicesTitleGroup['title_main'];
                $servicesTitleSub = $servicesTitleGroup['title_sub'];
            ?>

            <h1 class="block-title__main">
                <?= $servicesTitleMain; ?>
            </h1>

            <p class="block-title__sub">
                <?= $servicesTitleSub; ?>
            </p>
        </div>

        <p>
            <?= get_field('services_intro_paragraph'); ?>
        </p>

    
    </div>

</div>