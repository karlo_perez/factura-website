<!-- offset-intro class is used to identify the offset height(Y) of the image background's bottom curve -->
<div class="block--wfull single-post-service-intro offset-intro">

    <!-- <div class="caption-container">
        <h1 class="caption-main"><?php // echo the_title(); ?></h1>
        <p class="caption-sub">services</p>
    </div> -->

    <div class="image-container">
        <img src="<?= the_post_thumbnail(); ?>
        <!-- lint error here is caused by the "the_post_thumbnail();" WP bug wherein the (") cannot be properly parsed ;
        this comment prevents that saif error to leak over the next html tags -->
    </div>

    <?php
        $serviceHeroBackgroundGroup = get_field('service_hero_background_group');
        $heroBackgroundImage = $serviceHeroBackgroundGroup['hero_background_image'];
    ?>

    <div class="hero-background-container">
        <div class="hero-background-container__mask">
            <img src="<?= $heroBackgroundImage; ?>" alt="">
        </div>
    </div>

</div>