<div class="block--w1200 index-content">

    <div class="block-container">

        <div class="block-content">
            <?= the_content(); ?>
        </div>

    </div>

</div>