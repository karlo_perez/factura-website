<!-- Use get_template_part WP function instead of hardcoding this as this section should be available to all pages (except its directing page - Contact Page) and must be editable via WP Dashboard -->

<div class="block--wfull call-to-action">

    <div class="block-container">

        <?php
            // $ctaTitleGroup = get_field('cta_title_group');
            // $ctaTitleMain = $ctaTitleGroup['title_main'];
            // $ctaTitleSub = $ctaTitleGroup['title_sub'];

            // $ctaButtonGroup = get_field('cta_button_group');
            // $ctaButtonLabel = $ctaButtonGroup['button_label'];
            // $ctaButtonLink = $ctaButtonGroup['button_link'];
        ?>

        <div class="block-title">
            <h1 class="block-title__main text--light">
                <?php // echo $ctaTitleMain; ?>
                Let’s talk about your project
            </h1>

            <p class="block-title__sub text--light">
                <?php // echo $ctaTitleSub; ?>
                partnership
            </p>
        </div>

        <div class="button-group">
            <a href="<?php // echo $ctaButtonLink; ?>http://localhost:8080/facturasoftware/contact/" class="button button--light">
                <?php // echo $ctaButtonLabel; ?>
                Contact Us
            </a>
        </div>

    </div>

</div>