<div class="block--full index-intro">

    <div class="block-container">
    
    
        <div class="block-title block-title--left">


            <h1 class="block-title__main">
                <?= get_the_title(); ?>
            </h1>

            <p class="block-title__sub">
                <?php // echo $contactTitleSub; ?>
            </p>
        </div>

        <p>
            <?php //echo get_the_excerpt(); ?>
        </p>

    
    </div>

</div>