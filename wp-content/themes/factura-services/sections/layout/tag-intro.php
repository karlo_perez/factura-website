<div class="block--full tag-intro">

    <div class="block-container">


        <div class="block-title block-title--left">

            <?php 
                $url = $_SERVER['REQUEST_URI']; 
                $path = pathinfo($url);
                $selectedTag = $path['basename'];
            ?>

            <h1 class="block-title__main">
                <?= $selectedTag; ?>
            </h1>

            <p class="block-title__sub">
                selected_project_tag
            </p>

            <!-- FILTER TAGS to be placed on Tags page? -->
            <?php // echo do_shortcode( '
            // [searchandfilter 
            //     fields="post_tag" 
            //     headings="Filter by project type" 
            //     submit_label="Filter"
            // ]' ); ?>

        </div>
        

    </div>

</div>