<div class="block--full contact-intro">

    <div class="block-container">
    
    
        <div class="block-title block-title--left">

            <?php
                $contactTitleGroup = get_field('contact_title_group');
                $contactTitleMain = $contactTitleGroup['title_main'];
                $contactTitleSub = $contactTitleGroup['title_sub'];
            ?>

            <h1 class="block-title__main">
                <?= $contactTitleMain; ?>
            </h1>

            <p class="block-title__sub">
                <?= $contactTitleSub; ?>
            </p>
        </div>

        <p>
            <?= get_field('contact_intro_paragraph'); ?>
        </p>

    
    </div>

</div>