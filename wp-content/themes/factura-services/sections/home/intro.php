<div class="block--wfull home-intro offset-intro">
    
    <?php
        $introCaptionGroup = get_field('intro_caption_group');
        $introCaptionMain = $introCaptionGroup['caption_main'];
        $introCaptionSub = $introCaptionGroup['caption_sub'];

        $introHeroButtonGroup = get_field('intro_hero_button_group');
        $heroButtonGroupLeft = $introHeroButtonGroup['hero_button_group_left'];
        $heroButtonLeftLabel = $heroButtonGroupLeft['hero_button_label'];
        $heroButtonLeftLink = $heroButtonGroupLeft['hero_button_link'];
        $heroButtonGroupRight = $introHeroButtonGroup['hero_button_group_right'];
        $heroButtonRightLabel = $heroButtonGroupRight['hero_button_label'];
        $heroButtonRightLink = $heroButtonGroupRight['hero_button_link'];

        $introHeroBackgroundGroup = get_field('intro_hero_background_group');
        $heroBackgroundImage = $introHeroBackgroundGroup['hero_background_image'];
    ?>

    <div class="caption-container">
        <h1 class="caption-main"><?= $introCaptionMain; ?></h1>
        <p class="caption-sub"><?= $introCaptionSub; ?></p>
    </div>

    <div class="hero-button-container">
        <a class="hero-button-group--left" href="<?= $heroButtonLeftLink; ?>">
            <?= $heroButtonLeftLabel; ?>
        </a>
        <a class="hero-button-group--right" href="<?= $heroButtonRightLink; ?>">
            <?= $heroButtonRightLabel; ?>
        </a>
    </div>

    
    <div class="hero-background-container">
        <div class="hero-background-container__mask">
            <img src="<?php echo $heroBackgroundImage; ?>" alt="">
        </div>
    </div>
    
</div>