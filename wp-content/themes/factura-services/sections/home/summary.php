<div class="block--wfull summary">

    <div class="block-container">

        <div class="block-title">

            <?php
                $summaryTitleGroup = get_field('summary_title_group');
                $summaryTitleMain = $summaryTitleGroup['title_main'];
                $summaryTitleSub = $summaryTitleGroup['title_sub'];
            ?> 

            <h1 class="block-title__main">
                <?= $summaryTitleMain; ?>
            </h1>

            <p class="block-title__sub">
                <?= $summaryTitleSub; ?>
            </p>
        </div>


        <div id="flickity-summary" class="gallery js-flickity summary-gallery">

            <?php
                if ( have_rows('summary_gallery_cell') ) :
                    while ( have_rows('summary_gallery_cell') ) : the_row(); 
                    $summaryCellImage = get_sub_field('cell_image');
                    $summaryCellMain = get_sub_field('cell_main');
                    $summaryCellParagraph = get_sub_field('cell_paragraph');
            ?>

            <div class="gallery-cell carousel-cell summary-cell">

                <div class="summary-cell__image-container">
                    <img class="summary-cell__image" src="<?= $summaryCellImage; ?>" alt="">
                </div>

                <div class="summary-cell__caption-group">
                    <h3 class="summary-cell__caption-main">
                        <?= $summaryCellMain; ?>
                    </h3>

                    <p class="summary-cell__caption-paragraph">
                        <?= $summaryCellParagraph; ?>
                    </p>
                </div>

            </div>

            <?php
                endwhile;
            else:
            endif;
            ?>

        </div>

    </div>

</div>