<div class="block--wfull careers-content">

    <div class="block-container">


        <div class="block-content">

            <?php
                if ( have_rows('position_group') ) :
                    while ( have_rows('position_group') ) : the_row() ;

                    $positionTitle = get_sub_field('position_title');
                    $jobDescription = get_sub_field('job_description');
                    $jobHighlights = get_sub_field('job_highlights');
                    $keyResponsibilities = get_sub_field('key_responsibilities');
                    $qualifications = get_sub_field('qualifications');
                    $buttonLabel = get_sub_field('button_label');
                    $buttonLink = get_sub_field('button_link');
            ?>

                <details>
                    <summary>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.26 26.58">
                            <path class="a" d="M11.88,17.49a2,2,0,0,0,0-2.8l-2.81-2.8L.58,3.37A2,2,0,0,1,3.37.58L14.69,11.89a2,2,0,0,1,0,2.8l-2.81,2.8"/><path class="a" d="M.58,23.2l8.51-8.51a2,2,0,0,0,.58-1.42L11.37,15c.23.22,1.63,1.39-1,4.07L3.37,26A2,2,0,1,1,.58,23.2Z"/>
                        </svg>
                        <?= $positionTitle; ?>
                    </summary>
                 
                    <div><?= $jobDescription ?></div>
                    
                    <h3>Job Highlights</h3>
                    <div><?= $jobHighlights; ?></div>
                    
                    <h3>Key Responsibilities</h3>
                    <div><?= $keyResponsibilities; ?></div>

                    <h3>Qualifications</h3>
                    <div><?= $qualifications; ?></div>

                    <div class="button-group text--right">
                        <a href="<?php $buttonLink; ?>" class="button button--dark" target="blank">
                            <?= $buttonLabel; ?>
                        </a>
                    </div>

                </details>

            <?php
                    endwhile;
                else:
                endif;
            ?>

        </div>

    </div>

</div>

<script>
// const details = document.querySelectorAll('details');

// details.forEach(detail => {
//     const detailContent = detail.querySelector('div');
//     const detailClosedHeight = detail.scrollHeight;
//     // open the details to get the height of the content
//     detail.open = true;
//     // pass it to the the element as CSS property
//     detailContent.style.setProperty('--details-content-height-open', detailContent.scrollHeight + 'px');
//     detail.style.setProperty('--details-height-open', detailContent.scrollHeight + detailClosedHeight + 'px');
//     // close the details again
//     detail.open = false;

//     detailContent.style.setProperty('--details-content-height-closed', detailContent.scrollHeight + 'px');
//     detail.style.setProperty('--details-height-closed', detailClosedHeight + 'px');

//     detail.addEventListener('click', (ev) => {
//         const container = ev.target.parentElement;
//         // get time of transition from CSS property
//         const timeout = getComputedStyle(container.querySelector('div')).getPropertyValue('--details-transition-time');

//         // we can't use [open] as it will be only removed after the transition
//         container.classList.toggle('is--open');

//         // remove the open attribute once the transition is done, because otherwise we won't see the transition
//         if (container.open) {
//             ev.preventDefault();
//             setTimeout(function() {
//             container.open = false;
//             }, parseInt(timeout))
//         }
//     })
// });
</script>