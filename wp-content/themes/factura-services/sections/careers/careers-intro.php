<div class="block--full careers-intro">

    <div class="block-container">
    
    
        <div class="block-title block-title--left">

            <?php
                $careersTitleGroup = get_field('careers_title_group');
                $careersTitleMain = $careersTitleGroup['title_main'];
                $careersTitleSub = $careersTitleGroup['title_sub'];
            ?>

            <h1 class="block-title__main">
                <?= $careersTitleMain; ?>
            </h1>

            <p class="block-title__sub">
                <?= $careersTitleSub; ?>
            </p>
        </div>

        <p>
            <?= get_field('careers_intro_paragraph'); ?>
        </p>

    
    </div>

</div>