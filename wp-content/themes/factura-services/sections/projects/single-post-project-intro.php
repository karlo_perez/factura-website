<!-- offset-intro class is used to identify the offset height(Y) of the image background's bottom curve -->
<div class="block--wfull single-post-project-intro offset-intro">

    <div class="caption-container">
        <h1 class="caption-main"><?= the_title(); ?></h1>
        <p class="caption-sub">projects</p>
    </div>

    <?php
        $projectHeroBackgroundGroup = get_field('project_hero_background_group');
        $heroBackgroundImage = $projectHeroBackgroundGroup['hero_background_image'];
    ?>

    <div class="hero-background-container">
        <div class="hero-background-container__mask">
            <img src="<?php echo $heroBackgroundImage; ?>" alt="">
        </div>
    </div>

</div>