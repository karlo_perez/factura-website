<div class="block--full projects-intro">

    <div class="block-container">
    
    
        <div class="block-title block-title--left">

            <?php
                $projectsTitleGroup = get_field('projects_title_group');
                $projectsTitleMain = $projectsTitleGroup['title_main'];
                $projectsTitleSub = $projectsTitleGroup['title_sub'];
            ?>

            <h1 class="block-title__main">
                <?= $projectsTitleMain; ?>
            </h1>

            <p class="block-title__sub">
                <?= $projectsTitleSub; ?>
            </p>
        </div>

        <p>
            <?= get_field('projects_intro_paragraph'); ?>
        </p>

        <?php // echo do_shortcode( '
        // [searchandfilter 
        //     fields="post_tag" 
        //     headings="Filter by project type" 
        //     submit_label="Filter"
        // ]' ); ?>

        <form class="filter-posts" method="get" action="">
            <p class="form-block">
                <label>Filter Projects by Tag<br>
                    <span class="help-subject">
                
                    <?php
                        $tags = get_field("projects_filter_by_tag");
                        // this variable is passed unto projects-content.php query
                        $selectedTag = $_GET['filter-tag'];
                        // if ( !empty($selectedTag) ) :
                        //     $selectedTag = "All";
                        //     echo $selectedTag;
                        // endif;
                    ?>

                        <select name="filter-tag" class="filter-tag" aria-invalid="false" onchange="this.form.submit()">
                            <?php 
                            if ( $tags ) : ?>
                                <option value="uncategorized">All</option>
                                <?php
                                    foreach( $tags as $tag ) : 
                                    $tagSlug = esc_attr($tag->slug);
                                    $tagName = esc_attr($tag->name);
                                ?>
                                <option value="<?= $tagSlug; ?>"
                                    <?php 
                                        if ( isset($_GET['filter-tag']) && $_GET['filter-tag'] == $tagSlug ) : 
                                            echo 'selected="selected"';
                                        endif; 
                                    ?>
                                >
                                    <?= $tagName; ?>
                                </option>

                            <?php 
                                endforeach; 
                            else : 
                            endif;?>
                        </select>
                    
                    </span>
                </label>
            </p>
        </form>
    
    </div>

</div>