<div class="block--w1200 single-post-project-content">

    <div class="block-container">

        <div class="block-content">
            <?php
                the_content();
            ?>

            <p class="tag-list"> 
                <?php 
                    if ( has_tag() ) :
                        echo the_tags();
                    else :
                ?>
                    Tags: none
                <? endif; ?>
            </p>
        </div>
    
    </div>

</div>