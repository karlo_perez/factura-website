<!-- offset-intro class is used to identify the offset height(Y) of the image background's bottom curve -->
<div class="block--wfull single-post-team-intro offset-intro">

    <?php
        $teamHeroBackgroundGroup = get_field('team_hero_background_group');
        $heroBackgroundImage = $teamHeroBackgroundGroup['hero_background_image'];
    ?>

    <div class="caption-container">
        <h1 class="caption-main"><?= the_title(); ?></h1>
        <p class="caption-sub">introducing the team</p>
    </div>

    <div class="hero-background-container">
        <div class="hero-background-container__mask">
            <img src="<?php echo $heroBackgroundImage; ?>" alt="">
        </div>
    </div>

</div>