<!-- offset-intro class is used to identify the offset height(Y) of the image background's bottom curve -->
<div class="block--wfull company-intro offset-intro">

    <?php
        $companyTitleGroup = get_field('company_title_group');
        $companyTitleMain = $companyTitleGroup['title_main'];
        $companyTitleSub = $companyTitleGroup['title_sub'];
        $companyHeroBackgroundGroup = get_field('company_hero_background_group');
        $heroBackgroundImage = $companyHeroBackgroundGroup['hero_background_image'];
    ?>

    <div class="caption-container">
        <h1 class="caption-main"><?= $companyTitleMain; ?></h1>
        <p class="caption-sub"><?= $companyTitleSub; ?></p>
    </div>

    <div class="hero-background-container">
        <div class="hero-background-container__mask">
            <img src="<?php echo $heroBackgroundImage; ?>" alt="">
        </div>
    </div>

</div>