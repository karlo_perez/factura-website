<div class="block--w1200 single-post-team-content">

    <div class="block-container">

        <div class="block-content">
            <?php
                the_content();
            ?>
        </div>


        <div class="nav-post">  
            <span class="nav-post__previous">
                <?php 
                    previous_post_link($format = '%link', 
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.26 26.58">
                        <path class="a" d="M11.88,17.49a2,2,0,0,0,0-2.8l-2.81-2.8L.58,3.37A2,2,0,0,1,3.37.58L14.69,11.89a2,2,0,0,1,0,2.8l-2.81,2.8"/><path class="a" d="M.58,23.2l8.51-8.51a2,2,0,0,0,.58-1.42L11.37,15c.23.22,1.63,1.39-1,4.07L3.37,26A2,2,0,1,1,.58,23.2Z"/>
                        </svg>
                        %title',
                        $in_same_term = true); 
                ?>
            </span>
            <!-- <div class="nav-post__partition"></div> -->
            <span class="nav-post__next">
                <?php 
                    next_post_link($format = '%link', 
                        '%title
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 15.26 26.58">
                        <path class="a" d="M11.88,17.49a2,2,0,0,0,0-2.8l-2.81-2.8L.58,3.37A2,2,0,0,1,3.37.58L14.69,11.89a2,2,0,0,1,0,2.8l-2.81,2.8"/><path class="a" d="M.58,23.2l8.51-8.51a2,2,0,0,0,.58-1.42L11.37,15c.23.22,1.63,1.39-1,4.07L3.37,26A2,2,0,1,1,.58,23.2Z"/>
                        </svg>', 
                        $in_same_term = true); 
                ?>
            </span>
        </div>
    
    </div>

</div>