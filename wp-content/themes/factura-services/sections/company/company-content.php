<div class="block--w1200 company-content">

    <div class="block-container">

        <div class="block-content">
            <?php
                the_content();
            ?>
        </div>
    
    </div>

</div>