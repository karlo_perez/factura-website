<?php
/*
 * Template Name: Careers
*/
get_header(); 


@include 'sections/careers/careers-intro.php';
@require 'sections/careers/careers-content.php';
@include 'sections/layout/call-to-action.php';


get_footer(); ?>