<?php
/*
 * Template Name: Single Post Project
 * Template Post Type: Post
*/
get_header(); 


@include "sections/projects/single-post-project-intro.php";
@require "sections/projects/single-post-project-content.php";
@include "sections/projects/single-post-project-related-content.php";
@include 'sections/layout/call-to-action.php';


get_footer(); ?>