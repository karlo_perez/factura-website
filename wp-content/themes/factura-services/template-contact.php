<?php
/*
 * Template Name: Contact
*/
get_header(); 


@include 'sections/contact/contact-intro.php';
@require 'sections/contact/contact-content.php';


get_footer(); ?>