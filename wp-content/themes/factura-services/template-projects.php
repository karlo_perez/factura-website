<?php
/*
 * Template Name: Projects
*/
get_header(); 


@include 'sections/projects/projects-intro.php';
@require 'sections/projects/projects-content.php';
@include 'sections/layout/call-to-action.php';


get_footer(); ?>